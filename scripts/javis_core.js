"use strict";

class EventManager {

    constructor(){
        this.listeners = {};
        this.idCounter = 0;
    }

    on(eventname, callback){
        if(typeof eventname !== 'string'){
            console.error('eventname must be a string');
            return;
        }

        if(typeof callback !== 'function'){
            console.error('callback must be a function');
            return;
        }

        if(!this.listeners[eventname]){
            this.listeners[eventname] = {};
        }

        let callbackId;

        if(callback.___eventmanagerid___){
            if(listeners[eventname][callbackId]){
                console.error('callback is already listening to to event', eventname, 'skipping');
                return;
            }
        } else {
            callbackId = this.generateCallbackId();
            callback.___eventmanagerid___ = callbackId;
        }

        this.listeners[eventname][callbackId] = callback;
    }

    dispatch(eventname, eventdata){
        if(this.listeners[eventname]){
            for(let k of Object.keys(this.listeners[eventname])){
                this.listeners[eventname][k](eventdata);
            }
        }
    }

    generateCallbackId(){
        const id = this.idCounter;
        this.idCounter++;
        return id;
    }
}

class InstanceManager extends EventManager {
    static ID_COUNTER = 0;
    static instances = {};

    constructor(){
        super();
        this.id = InstanceManager.registerInstance(this);
    }

    static registerInstance(instance){
        const myId = InstanceManager.generateId();
        InstanceManager.instances[myId] = instance;
        return myId;
    }

    static generateId(){
        const id = InstanceManager.ID_COUNTER;
        InstanceManager.ID_COUNTER++;
        return id;
    }

    static getInstance(id){
        return InstanceManager.instances[id];
    }
}


class Javis extends InstanceManager {
    
    /* events:

        statechange (when isRunning or isPaused changes)

        streamchange (when currentStream changes)

    */

    static VERSION = '1.2';
    static ABOUT_URL = 'https://javis.flaffipony.rocks';

    constructor(container){
        super()
        if(this.checkCompatibility() === false){
            //TODO: show message on screen
            return;
        }

        if(container instanceof HTMLElement === false){
            this.error('invalid container!', container);
            return;
        }

        this.dom = $(container);

        this.isRunning = false; /* if is running a stream */
        this.isPaused = false; /* if stream is running but paused (only available for stream type file) */
        this.runType = undefined; /* "recording" (microphone input), "file" (playing file or url), "connector" (receiving ia connector), undefined if not running */

        this.audioContext = new AudioContext();

        this.audioDestinationNode = this.audioContext.createGain();
        this.audioDestinationNode.gain.value = 1;
        this.audioDestinationNode.connect(this.audioContext.destination);


        this.audioSourceNode = this.audioContext.createGain();
        this.audioSourceNode.gain.value = 1;

        this.outputStreamNode = this.audioContext.createMediaStreamDestination();
        this.audioSourceNode.connect(this.outputStreamNode);

        this.bypassNode = this.audioContext.createGain();
        this.bypassNode.gain.value = 0;
        this.audioSourceNode.connect(this.bypassNode);
        this.bypassNode.connect(this.audioDestinationNode);

        this.config = Javis_Config.DEFAULT.clone();
        this.ui = new Javis_UserInterface(this);
        this.visualizer = new Javis_Visualizer(this);
        this.connector = new Javis_Connector(this);

        this.ui.configurator.on('change', ()=>{
            this.setVolume();
        });

        this.config.on('change', ()=>{
            this.ui.configurator.refresh();
        });
    }

    startFile(file){
        this.audioContext.resume();
        if(file instanceof File === false){
            this.error('argument must be a File!');
            return;
        }

        let blob = URL.createObjectURL(file);
        this.loadURL(blob, file.type)

        this.visualizer.buildId3Tags({url: blob, file: file, title: file.name.match(/([\w\-\s]*)\.[\w\-\s]*/)[1]});
    }

    startUrl(url, overrideMime){  
        this.audioContext.resume();      
        this.loadURL(url,overrideMime);
        
        let urlAlternativeName = url.toString().match(/.*[^\w\-\.\s]([\w\-\.\s]+)?/)[1].match(/([\w\-\s]*)\.[\w\-\s]*/)[1];

        this.visualizer.buildId3Tags({url: url, title: urlAlternativeName});
    }

    loadURL(url, overrideMime) {
        console.log('loading url ', url);

        this.audioElement = document.createElement('audio');

        let once = true;

        this.audioElement.onloadeddata = ()=>{
            if(once){
                once = false;
                
                let stream;
                if(this.audioElement.captureStream){
                    stream = this.audioElement.captureStream();
                } else if (this.audioElement.mozCaptureStream) {
                    stream = this.audioElement.mozCaptureStream();
                } else {
                    alert('HTMLAudioElement.captureStream() not supported!');
                    return;
                }

                this.audioElement.play();
                this._runFileStream(stream);
            }
        }

        this.audioElement.onended = ()=>{
            //this.stopStream();
        }

        this.audioElement.onerror = (evt)=>{
            this.error('cannot load audio element', evt);
        }

        this.audioElement.src = url;
    }

    /*
        callback(status)
        "status = true" => user allowed microphone
        "status = false" user did not allow;
    */
    startMicrophoneRecording(callback){
        this.audioContext.resume();
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia){
            navigator.mediaDevices.getUserMedia({audio:true}).then((stream)=>{
                if(typeof callback === 'function'){
                    callback(true);
                }
                this._runRecordingStream(stream);

                this.visualizer.buildId3Tags({title: 'Recording'});
            }).catch((e)=>{
                if(typeof callback === 'function'){
                    callback(false);
                }
                this.error('Error capturing audio.', e)
                this.ui.showMessage('Error capturing audio.')
            });
        } else {
            this.error('browser is not supporting microphone')
            this.ui.showMessage('browser is not supporting microphone')
        }
    }


    startConnectorReceiving(stream){
        this.audioContext.resume();
        this._runConnectorStream(stream)
    }


    _runRecordingStream(stream){
        this._runStream(stream, 'recording');
    }

    _runFileStream(stream){
        this._runStream(stream, 'file');
    }

    _runConnectorStream(stream){
        this._runStream(stream, 'connector');
    }

    _runStream(stream, type){
        console.log('running stream', stream, type);

        if(this.isRunning){
            this.stopStream();
        }

        this.dispatch('streamchange', stream);

        this.currentStream = stream;
        
        this.currentStreamSource = this.audioContext.createMediaStreamSource(stream);
        this.currentStreamSource.connect(this.audioSourceNode);

        this.audioSourceNode.gain.value = 1;

        this.runType = type;
        this.isRunning = true;
        this.isPaused = false;

        this.setVolume();

        this.dispatch('statechange')
    }

    stopStream(){
        this.isPaused = false;
        this.isRunning = false;
        this.runType = undefined;

        if(this.currentStreamSource){
            this.currentStreamSource.disconnect();
            if(this.runType === 'file'){
                /* try to stop playing the file */
                try {
                    this.currentStreamSource.stop();
                } catch (thrown){}
            }
            this.currentStreamSource = undefined;
        }

        this.currentStream = undefined

        this.dispatch('streamchange')
        this.dispatch('statechange')
    }

    /* pauses file stream, or mutes connector or microphone stream */
    pauseStream(){
        if(! this.isRunning){
            this.error('Cannot pause stream, it is not running!');
            return;
        }

        if(this.isPaused){
            this.error('Cannot pause a stream that is paused!');
        } else {            
            if(this.runType === 'file'){
                this.audioElement.pause();
            } else {
                this.audioSourceNode.gain.value = 0;
            }

            this.isPaused = true;

            this.dispatch('statechange')
        }
    }

    /* resumes file stream, or unmutes connector or microphone stream */
    resumeStream(){
        if(! this.isRunning){
            this.error('Cannot resume stream, it is not running!');
            return;
        }

        if(!this.isPaused){
            this.error('Cannot resume a stream that is paused!');
        } else {            
            if(this.runType === 'file'){
                this.audioElement.play();
            } else {
                this.audioSourceNode.gain.value = 1;
            }
            
            this.isPaused = false;

            this.dispatch('statechange')
        }
    }

    restartStream(){
        if(this.runType === 'file'){
            console.log('restarting stream/file...');

            if(this.isRunning){
                this.audioElement.currentTime = 0;
            } else {
                this.error('Cannot restart stream, it is not running!');
            }
        } else {
            console.info('Cannot restart stream, it is a microphone or connector stream');
        }
    }

    setVolume(){
        if(this.runType === 'file' || this.runType === 'url'){
            /* set volume through AudioElement because of Chrome behavior */
            if(navigator.userAgent.toLowerCase().indexOf('firefox') >= 0){
                if(this.audioElement){
                    this.audioElement.volume = 1;
                }
                this.bypassNode.gain.value = this.getConfig('volume');                
            } else {
                if(this.audioElement){
                    this.audioElement.volume = this.getConfig('volume');
                }
                this.bypassNode.gain.value = 0;
            }
        } else {
            if(this.audioElement){
                this.audioElement.volume = 0;
            }
            this.bypassNode.gain.value = this.getConfig('volume');            
        }
    }


    getConfig(name){
        return this.config.getValue(name, this);
    }

    setConfig(name, value){
        return this.config.setValue(name, value);
    }

    // TODO: more tests?
    checkCompatibility(){                
        if (! window.AudioContext) {
            if (window.webkitAudioContext) {
                window.AudioContext = window.webkitAudioContext;
            } else {
                alert('Seems like the webbrowser does not support AudioContext!');
            }
        }

        if(! window.RTCPeerConnection){
          alert('Seems like the webbrowser does not support Web RTC!');
        }
    }

    error(...args){
        console.error.apply(window, ['JAVIS Error:'].concat(args));
    }
}
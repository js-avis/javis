

function Javis_Dialog(params, container, callback){
    if(!params){
        console.error('missing params!')
        return null
    }
    if(!container){
        console.error('missing container!')
        return null
    }
    if(!callback){
        console.warn('missing callback!')
    } else if(typeof callback !== 'function'){
        console.warn('callback must be a function!')
    }
    console.log('creating new Javis_Dialog in container', container)
    this.container = container
    this.callback = callback
    this.id = Javis_Dialog.generateId()

    
    build(params, container, this.id)
    
    function build(params, container, instance_id){
        console.log('building Javis_Dialog... with params', params)
        let html = '<div dialog-id="'+instance_id+'" class="javis_dialog" dialog-type="'+params.type+'"><div>'
        if(params.title) html += '<h2 class="javis_dialog_title">'+params.title+'</h2>'
        if(params.type === Javis_Dialog.TYPE.INPUT){
            for(let input of params.inputs){
                let uqid = uniqueID()
                html += '<div class="javis_dialog_row">'+
                '<input id="'+uqid+'" type="'+input.type+'" data-inputname="'+input.name+'" value="'+(input.value?input.value:'')+'">'+
                '<label for="'+uqid+'">'+input.short+'</label>'+
                '<p class="javis_dialog_description">'+input.description+'</p>'+
                '</div>'
            }
            html += '<div class="javis_dialog_row">'+
                '<span class="javis_button" onclick="Javis_Dialog.getInstance('+instance_id+').gatherInput()">Ok</span>'+
                '</div>'
        } else if(params.type === Javis_Dialog.TYPE.OPTION_BUTTONS){
            for(let option of params.options){
                html += '<div class="javis_dialog_row">'+
                '<span class="javis_button" data-optionvalue="'+option.value+'" onclick="Javis_Dialog.getInstance('+instance_id+').choose($(this).data().optionvalue)">'+(option.short?option.short:option.value)+'</span>'+
                '<p class="javis_dialog_description">'+(option.description?option.description:'')+'</p>'+
                '</div>'
            }
        } else if(params.type === Javis_Dialog.TYPE.MESSAGE){
            html += '<p>'+params.text+
                '</p>'
        }
        if(params.no_cancel !== false){
          html += '<span class="javis_dialog_close" onclick="Javis_Dialog.getInstance('+instance_id+').cancel()"></span>'
        }
        html += '</div></div>'
        let $html = $(html)
        $(container).append($html)
        $html.find('input:first-of-type').focus()
        //console.log('build finished', $(this.container))
    }

    function uniqueID() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
    }

    this.gatherInput = function(){
        let ret = {}
        $('[dialog-id="'+this.id+'"] .javis_dialog_row input').each((index, elem)=>{
            if($(elem).attr('type') === 'file'){
                ret[$(elem).data().inputname] = $(elem).get(0).files;
            } else {
                ret[$(elem).data().inputname] = $(elem).val();
            }
        })
        $('[dialog-id="'+this.id+'"]').remove()
        console.log('dialog gathered input', ret)
        this.callback(ret)
    }
    
    this.choose = function(value){
        $('[dialog-id="'+this.id+'"]').remove()
        console.log('dialog chose option', value)
        this.callback(value)
    }

    this.cancel = function(){
        $('[dialog-id="'+this.id+'"]').remove()
        console.log('dialog canceled')
        this.callback(false)
    }
    
    Javis_Dialog.registerInstance(this)
}

Javis_Dialog.TYPE = {
    OPTION_BUTTONS: 'option_buttons',
    INPUT: 'input',
    MESSAGE: 'message'
}

Javis_Dialog.instances = {}

Javis_Dialog.registerInstance = function (instance){
    if(instance instanceof Javis_Dialog){
        Javis_Dialog.instances[instance.id] = instance
    } else {
        console.error('cannot register a non-Javis_Dialog-instance!')
    }
}

Javis_Dialog.getInstance = function (id){
    if(Javis_Dialog.instances[id] instanceof Javis_Dialog){
        return Javis_Dialog.instances[id]
    } else {
        console.error('Javis_Dialog instance', id, 'not found!')
    }
}

Javis_Dialog.id_counter = 0

Javis_Dialog.generateId = function (){
    Javis_Dialog.id_counter = Javis_Dialog.id_counter+1
    return Javis_Dialog.id_counter
}

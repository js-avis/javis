/* includes functions that both sender and receiver need */
class Javis_Connector_Sub extends EventManager {
    constructor(connector, key){
        super();

        this.connector = connector;

        this.isConnected = false; // true if dataChannel is open and hello handshake has happened
        this.directConnectionStable = false; // true if ICE search was successful and direct web rtc connection is established

        this.key = key;

        this.SIGNAL_URL = "ws://localhost:3750";//"https://javis.flaffipony.rocks/api/";

        this.sendMessageQueue = [];

        this.checkSendMessageQueueInterval = setInterval(()=>{
            this.checkSendMessageQueue();
        }, 100);

        this.createConnection();

        this.KEEP_ALIVE_NOTIFY_INTERVAL = 500000; // TODO change when in production
    }

    createConnection(){
        this.connection = new RTCPeerConnection({
            iceServers: [{urls: ["stun:stun.l.google.com:19302", "stun:stun-1.example.com"]}] //stun.stunprotocol.org stun:stun.example.com
        });

        this.connection.onicecandidate = (evt)=>{this.onIceCandidate(evt)};

        this.connection.onnegotiationneeded = ()=>{//used not for the initial negotiation, but for negotiations after successfully connected once. This is necessary since adding a stream/track requires renegotiation.
            if(this.isConnected){                
                this.connection.createOffer().then(description => {
                    this.connection.setLocalDescription(description);
                    this.sendDataMessage('offer', description);
                })
            } else {
                console.warn('web rtc negotiation needed but not connected yet');
            }
        }

        this.connection.onconnectionstatechange = (evt)=>{
            console.error('connection change', this.connection.connectionState)
            if(this.connection.connectionState === 'connected'){
                this.directConnectionStable = true;
                this.dispatch('direct-connection-open');
            } else {
                this.directConnectionStable = false;
                this.dispatch('direct-connection-close');
            }
        }

        this.connection.oniceconnectionstatechange = (evt)=>{
            console.error('ice connection state change', this.connection.iceConnectionState);
        }

        this.socket = new WebSocket(this.SIGNAL_URL, 'javis-protocol');
        console.log(this.SIGNAL_URL);

        this.socket.onopen = ()=>{
            this.onSocketOpen();
        };

        this.socket.onclose = (code, description)=>{
            this.onSocketClose(code, description);
        };

        this.socket.onmessage = (evt)=>{
            this.onSocketMessage(evt);
        };

        setTimeout(()=>{
            setInterval(()=>{
                if (this.socket.readyState === WebSocket.OPEN) {
                    this.socketSend('keep-alive', {
                        sessionKey: this.key,
                        transmissionId: this.transmissionId
                    })
                }
            }, this.KEEP_ALIVE_NOTIFY_INTERVAL)
        }, this.KEEP_ALIVE_NOTIFY_INTERVAL)
    }

    onSocketOpen(){
        console.log('websocket open');
    }

    onSocketClose(code, description){        
        console.log('websocket closed', code, description);
    }

    onSocketMessage(evt){
        let msg;
        try {
            msg = JSON.parse(evt.data);
        } catch (ex){
            console.error('error parsing message', ex);
        }

        if(msg){
            console.log('received websocket message', msg);

            switch(msg.type){
                case 'ice-candidate': {
                    console.log('got ice candidate:', msg.content.candidate, this.connection.iceConnectionState, this.connection.connectionState)
                    this.connection.addIceCandidate(msg.content.candidate);
                }; break;

                case 'error': {
                    console.error('Socket reported error:', msg.content)
                }; break;

                default : {
                    this.dispatch('socket-message', msg)
                }
            }
        }
    }

    socketSend(type, content){
        if(this.socket.readyState !== WebSocket.OPEN){
            // delay
            setTimeout(()=>{
                this.socketSend(type, content)
            }, 5000);
            return;
        }
        this.socket.send(JSON.stringify({
            type: type,
            content: content
        }));
    }

    initDataChannelListeners(){
        this.dataChannel.onopen = (evt)=>{this.onDataChannelOpen(evt)};
        this.dataChannel.onclose = (evt)=>{this.onDataChannelClose(evt)};

        this.dataChannel.onmessage = (evt)=>{this.onDataChannelMessage(evt)};

        this.sendDataMessage('hello', this.type);
    }

    onDataChannelOpen(evt){
        console.info('ctrl channel open');

        this.sendDataMessage('hello', this.type);
    }

    onDataChannelClose(evt){
        console.info('ctrl channel closed');

        this.isConnected = false;

        this.dispatch('disconnected');
    }

    onDataChannelMessage(evt){
        console.log('onDataChannelMessage', evt.data);

        let parsed;

        // check for compatibility
        try {
            parsed = JSON.parse(evt.data);

            if(parsed['javis-version'] !== this.connector.core.VERSION){
                throw new Error('incompatible connector');
            }

        } catch (ex){
            console.error('incompatible remote connector, closing connection');
            this.connector.core.ui.error('Cannot connect, incompatible versions!');
            this.dataChannel.close();

            return;
        }

        switch(parsed.type){
            case 'hello': {
                console.log('received hello');
        
                this.isConnected = true;
                this.isRenegotiating = false;

                this.dispatch('connected');
            }; break;
            case 'offer': {
                if(!this.isRenegotiating){
                    this.isRenegotiating = true;

                    console.log('received renegotiation offer message', parsed);

                    this.connection.setRemoteDescription(parsed.content)
                    
                    this.connection.createAnswer().then((description) => {
                        this.connection.setLocalDescription(description);
                        this.sendDataMessage('answer', description);
                    })
                } else {
                    console.log('skipped offer because is renegotiating', parsed.content)
                }
            }; break;
            case 'answer': {
                
                console.log('received renegotiation answer message', parsed);

                this.connection.setRemoteDescription(parsed.content);

            }; break;
            default: {
                //received unknown message over data channel

                this.dispatch('message', {
                    type: parsed.type,
                    content: parsed.content
                });
            }
        }
    }

    checkSendMessageQueue(){
        if(this.dataChannel && this.dataChannel.readyState === 'open'){
            while(this.sendMessageQueue.length > 0){
                let entry = this.sendMessageQueue.shift();

                this.sendDataMessage(entry.type, entry.content);
            }
        }
    }

    sendDataMessage(type, content){
        if(!this.dataChannel || this.dataChannel.readyState !== 'open'){
            this.sendMessageQueue.push({
                type: type,
                content: content
            });
            console.log('queuing data message', type, content);
            return;
        }

        console.log('sending data message', type, content);

        this.dataChannel.send(JSON.stringify({
            'javis-version': this.connector.core.VERSION,
            type: type,
            content: content
        }));
    }

    sendDataMedia(type, blobUrl){
        console.log('sending data media', type, blobUrl);

        let channel = this.connection.createDataChannel('media')

        channel.onopen = ()=>{
            channel.send(type);
            channel.send(blobUrl);
        }

        channel.onmessage = (msg)=>{
            console.log('received data media channel message', msg)
            if(msg === 'complete'){
                channel.close();
                channel = undefined;
            }
        }
    }

    onIceCandidate(evt){
        if(!evt.candidate){
            // ignore empty candidate
            return
        }
        console.log('found ice candidate', evt.candidate);

        this.shareIceCandidate(evt.candidate);
    }

    shareIceCandidate(candidate){
        if(!this.key){
            // delay (until key is available)
            console.warn('delaying shareIceCandidate')
            setTimeout(()=>{
                this.shareIceCandidate(candidate)
            }, 200);
            return;
        }

        let data = {
            sessionKey: this.key,
            transmissionId: this.transmissionId,
            candidate: candidate
        }

        console.log('shareIceCandidate', data)

        this.socketSend('ice-candidate', data);
    }

    close(){

        if(this.dataChannel){
            this.dataChannel.close();
            this.dataChannel = undefined;
        }
        
        this.connection.close();
        this.connection = undefined;

        this.connector = undefined;
    }
}


class Javis_Connector_Sender extends Javis_Connector_Sub {

    constructor(connector, onKeyCallback){
        super(connector);

        this.type = 'registrar';
        
        // if we dont do this, ice is not even being initiated ?!?
        this.dataChannel = this.connection.createDataChannel('ctrl');
        this.initDataChannelListeners();

        this.on('socket-message', (msg)=>{
            switch(msg.type){
                case 'session-key': {
                    this.key = msg.content;
                    if(typeof onKeyCallback === 'function'){
                        onKeyCallback(msg.content);
                    }
                }; break;

                case 'new-transmission': {
                    this.handleNewTransmission(msg.content.transmissionId)
                }; break;

                case 'transmission-answer': {
                    this.handleReceiverDescription(msg.content.answerDescription, msg.content.transmissionId)
                }; break;

                default : {
                    console.log('Unsupported socket message type', msg.type)
                }
            }
        })

        this.on('direct-connection-open', ()=>{
            console.log('direct connection is open!');
            /*this.dataChannel = this.connection.createDataChannel('ctrl');
            this.initDataChannelListeners();*/
        })

        this.on('direct-connection-close', ()=>{            
            // TODO, what should we do then?
        })
    }

    onSocketOpen(){        
        this.socketSend('create-session');
    }
    
    handleNewTransmission(transmissionId){
        console.log('new transmission', transmissionId);
        this.transmissionId = transmissionId // TODO rewrite this dirty fix (it wont work with multiple transmitions at same time)

        this.connection.createOffer().then((description)=>{
            this.connection.setLocalDescription(description);

            this.socketSend('transmission-offer', {
                sessionKey: this.key,
                transmissionId: transmissionId,
                offerDescription: description
            });
        }).catch((err)=>{
            console.error('Failed to create RTC description', err);
        })
    }

    handleReceiverDescription(description, transmissionId){
        console.log('got receiver description:', description, transmissionId);
        this.connection.setRemoteDescription(description);
    }
}

class Javis_Connector_Receiver extends Javis_Connector_Sub {

    constructor(connector, key){
        super(connector, key);

        this.type = 'answer';

        this.mediaChannels = {};

        this.connection.ondatachannel = (evt)=>{
            console.info('ondatachannel', evt);
            if(evt.channel.label === 'ctrl'){
                this.dataChannel = evt.channel;

                this.initDataChannelListeners();
            } else if (evt.channel.label === 'media'){
                this.handleMediaTransmission(evt.channel);
            }
        }

        this.on('socket-message', (msg)=>{
            switch(msg.type){
                case 'transmission-id' : {
                    console.log('transmissionId', msg.content);
                    this.transmissionId = msg.content;
                }; break;

                case 'transmission-offer': {
                    this.handleSenderDescription(msg.content.offerDescription, msg.content.transmissionId);
                }; break;

                default : {
                    console.log('Unsupported socket message type', msg.type)
                }
            }
        })
    }

    onSocketOpen(){        
        this.socketSend('create-transmission', {
            sessionKey: this.key
        });
    }

    handleSenderDescription(description, transmissionId){

        console.log('got sender description:', description, transmissionId);
        this.connection.setRemoteDescription(description);

        this.connection.createAnswer().then((desc)=>{
            this.connection.setLocalDescription(desc);

            this.socketSend('transmission-answer', {
                sessionKey: this.key,
                transmissionId: transmissionId,
                answerDescription: desc
            });
        })
    }

    handleMediaTransmission(channel){
        if(!this.mediaChannels[channel.id]){
            this.mediaChannels[channel.id] = {
                channel: channel
            }
        }

        channel.onmessage = (evt)=>{
            console.log('got media transmission data', evt.data)

            if(! this.mediaChannels[channel.id].type){
                this.mediaChannels[channel.id].type = evt.data
            } else {
                this.mediaChannels[channel.id].data = evt.data

                this.connector.onReceiveMediaTransmission(this.mediaChannels[channel.id].type, this.mediaChannels[channel.id].data);
            }
        }
    }
}

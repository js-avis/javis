"use strict";

/*

    UI is responsible for everything except the actual visualization.

*/

class Javis_UserInterface {
    
    constructor(core){
        this.core = core

        this.dom = $('<div class="javis">');
        $(core.dom).append(this.dom);

        this.settingsContainer = $('<div class="javis_settings">');
        this.dom.append(this.settingsContainer);

        this.configurator = new Javis_Configurator(this);

        this.controlsContainer = $('<div class="javis_control_bar_container">');

        this.dom.append(this.controlsContainer);

        this.messageContainer = $('<div class="message_container">');
        this.dom.append(this.messageContainer);


        let that = this;

        /* controls */

        this.controlsContainer.html('');

        this.controlsContainer.append($('<div class="javis_control_bar">'));

        this.dom.on('click', (evt)=>{
            let target = evt.originalEvent.target;

            this.core.visualizer.dom.children().each((i,elem)=>{
                if(elem === target){
                    this.toggleControlBar();
                }
            });
        });
        


        this.addControlGroup(
            [{
                hint: 'Load file',
                icon: 'audio_file',
                onclick: ()=>{
                    this.openFileDialog()
                }
            },{
                hint: 'Live record',
                icon: 'record',
                onclick: ()=>{
                    this.openRecordDialog()
                }
            },
            {
                hint: 'Load url',
                icon: 'url',
                onclick: ()=>{
                    this.openUrlDialog()
                }
            }]
        );

        this.addControlGroup(
            [{
                hint: 'Play / Pause',
                icon: 'play_pause',
                onclick: ()=>{
                    this.togglePlayPause()
                }
            },
            {
                hint: 'Restart',
                icon: 'restart',
                onclick: ()=>{
                    this.restart()
                }
            }]
        );

        this.addControlGroup(
            [{
                hint: 'Settings',
                icon: 'settings',
                onclick: ()=>{
                    this.toggleSettings()
                }
            },{
                hint: 'Help',
                icon: 'help',
                onclick: ()=>{
                    this.openDocs()
                }
            }]
        );


        this.controlsContainer.addClass('open');
    }

    addControlGroup(config){

        let $group = $('<div class="javis_control_bar_group">');
        this.controlsContainer.find('.javis_control_bar').append($group);

        for(let entry of config){
            if(entry.is_group){
                let $entry = $('<div class="javis_control_bar_entry_group">');
                $group.append($entry);

                if(entry.hint){
                    $entry.attr('title', entry.hint);
                }

                $entry.on('mouseleave', ()=>{
                    if(! this.isMobile()){
                        $entry.removeClass('toggled');
                    }
                });

                let entryGroupHead = $('<div class="entry_group_head">');
                $entry.append(entryGroupHead);
                entryGroupHead.on('click', ()=>{
                    $entry.toggleClass('toggled');
                });

                entryGroupHead.html('<span class="javis_controls_icon javis_icon_' + entry.icon + '">');

                let entryGroupBody = $('<div class="entry_group_body">');
                $entry.append(entryGroupBody);

                for(let inner of entry.entries){
                    let $inner = $('<div class="javis_control_bar_entry">');
                    entryGroupBody.append($inner);

                    if(inner.hint){
                        $inner.attr('title', inner.hint);
                    }

                    $inner.html('<span class="javis_controls_icon javis_icon_' + inner.icon + '">');

                    $inner.on('click', inner.onclick)

                    if(typeof inner.afterBuild === 'function'){
                        inner.afterBuild($inner);
                    }
                }
            } else {
                let $entry = $('<div class="javis_control_bar_entry">');
                $group.append($entry);

                if(entry.hint){
                    $entry.attr('title', entry.hint);
                }

                $entry.html('<span class="javis_controls_icon javis_icon_' + entry.icon + '">');

                $entry.on('click', entry.onclick)

                if(typeof entry.afterBuild === 'function'){
                    entry.afterBuild($entry);
                }
            }
        }
    }


    toggleControlBar(e){
        $(this.controlsContainer).toggleClass('open')
    }

    openFileDialog(){
        new Javis_Dialog({
            title: 'Select file to play:',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'file',
                type: 'file',
                short: 'File',
                description: 'Only accepts sound files!'
            }]
        }, this.dom, (values)=>{
            if(values !== false){
                if(values.file instanceof FileList && values.file[0] instanceof File){
                    this.core.startFile(values.file[0]);
                } else {
                    this.showMessage('no file selected');
                }
            }
        })
    }

    openRecordDialog(){
        let dialog;

        this.core.startMicrophoneRecording((status)=>{
            if(dialog){
                dialog.cancel();
            }
            if(!status){
                new Javis_Dialog({
                    title: 'Error',
                    type: Javis_Dialog.TYPE.MESSAGE,
                    text: 'You did not give us the permission, but it is required to record.'
                }, this.dom, ()=>{});
            }
        })

        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia){
            dialog = new Javis_Dialog({
                title: 'Allow access to microphone',
                type: Javis_Dialog.TYPE.MESSAGE,
                text: 'Your browser will now ask you to give this site permission to use your microphone.<br>If you use windows and your soundcard supports it, you can also use your speaker output as microphone input, then you can visualize e.g. youtube music.'
            }, this.dom, ()=>{});
        }
    }

    openUrlDialog(){
        new Javis_Dialog({
            title: 'Enter link of audio file to load:',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'url',
                type: 'url',
                short: 'URL',
                description: 'Make sure that the server allows cross origin request. Most websites won\'t!'
            }]
        }, this.dom, (values)=>{
            if(values !== false){
                if(typeof values.url === 'string' && values.url.match(/http(s?):\/\/.*/)){
                    this.core.startUrl(values.url);
                } else {
                    this.message('invalid url');
                }
            }
        })
    }

    togglePlayPause(){
        if(this.core.isRunning){
            if(this.core.isPaused){
                this.core.resumeStream();
            } else {
                this.core.pauseStream();
            }
        } else {
            console.log('you can\'t switch play pause when not running');
        }
    }

    toggleSettings(){
        console.log('toggleSettings', $(this.settingsContainer))
        if($(this.dom).attr('settings-state') == 'show'){
            this.closeSettings()
        } else {
            this.openSettings()
        }
    }
    
    closeSettings(){
        $(this.settingsContainer).css('max-height', $(this.dom).height())
        $(this.dom).attr('settings-state', 'out')
        setTimeout(()=>{
            $(this.dom).attr('settings-state', 'hide')
        }, 500)
    }
    
    openSettings(){
        $(this.dom).attr('settings-state', 'in')
        setTimeout(()=>{
            $(this.dom).attr('settings-state', 'show')
            $(this.settingsContainer).css('max-height','')
        }, 500)
    }

    restart(){
        this.core.restartStream();
    }

    openSaveConfigDialog(){
        new Javis_Dialog({
            title: 'Save config locally',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'savename',
                type: 'text',
                short: 'Name'
            }]
        }, this.dom, (values)=>{
            if(values !== false && this.saveConfiguration(this.core.config, values.savename)){
                this.showMessage('Configuration saved as "'+values.savename+'"')
            }
        })
    }

    saveConfiguration(config, name){
        if(! this.configurator.saveLocalConfig(config, name)){
            this.showMessage('Could not save local config')
        }
    }

    openLoadConfigDialog(){
        let local_configs = this.configurator.getLocalConfigs()
        if(local_configs === false) return
        new Javis_Dialog({
            title: 'Select local config to load',
            type: Javis_Dialog.TYPE.OPTION_BUTTONS,
            options: (function(){
                let arr = []
                for(let config of local_configs){
                    arr.push({
                        value: config.name,
                        short: config.name
                    })
                }
                return arr
            })()
        }, this.dom, (value)=>{
            if(value !== false){
                for(let config of local_configs){
                    if(config.name === value.toString()) return this.loadConfiguration(config.the_config)
                }
                console.error('selected configuration was not found. Dialog messed it up. Selected:', value)
                this.showMessage('The selected configuration does not exist')
            }
        })
    }

    loadConfiguration(config){
        this.core.ui.configurator.setConfig(config);
    }

    openDocs(){
        let win = window.open(Javis.ABOUT_URL)
    }


    isMobile(){
        let ret = $(this.dom).width() <= 768 || $(this.dom).height() <= 768
        return ret
    }


    showMessage(...args){
        let message = $('<div class="javis_message">');

        for(let a of args){
            message.append( $('<p>').html(a.toString()) );
        }

        this.messageContainer.append(message);

        setTimeout(()=>{
            message.remove();
        }, 1000 * 10);
    }

    error(...args){
        this.showMessage(args);
    }
}

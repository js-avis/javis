"use strict";

class Javis_Visualizer extends EventManager {
    
    constructor(core){
        super();
        
        this.core = core
        this.analyzer = new Javis_Analyzer(this)

        this.dom = $('<div class="javis_visualizer">')
        this.core.ui.dom.append(this.dom)
        
        this.canvas = $('<canvas class="canvas" width="1000" height="1000">');
        this.canvas.appendTo(this.dom);

        this.canvasMin = 1; // minimum of canvas width and height

        this.video = $('<video class="video">').hide();
        this.video.appendTo(this.dom);

        this.background = this.dom;

        this.innerHolder = $('<div class="inner_holder">');
        this.innerHolder.appendTo(this.dom)

        this.inner = $('<div class="inner">');
        this.inner.appendTo(this.innerHolder);

        this.tags = $('<div class="tags">');
        this.tags.appendTo(this.innerHolder);

        this.tags.html(
            '<span class="artist" style="font-size: 100%"></span>'+
            '<span class="title" style="font-weight: bold; font-size: 100%"></span>'+
            '<span class="album" style="font-size: 100%"></span>'
        );

        this.core.ui.addControlGroup(
            [{
                is_group: true,
                hint: 'Select preset',
                icon: 'presets',
                entries: (function (presets, that){
                    let arr = []
                    for(let preset of presets){
                        arr.push({
                            hint: preset,
                            icon: 'preset_' + preset.toLowerCase(),
                            onclick: ()=>{
                                that.selectPreset(preset)
                            }
                        })
                    }
                    return arr;
                })((()=>{
                    let presets = this.core.ui.configurator.getPresetList();

                    console.log(presets,presets.indexOf('DEFAULT'))                    

                    if(presets.indexOf('DEFAULT') >= 0){
                        presets.splice(presets.indexOf('DEFAULT'), 1);
                    }

                    console.log(presets)

                    return presets;
                })(), this)
            },{
                is_group: true,
                icon: 'paint',
                hint: 'Styling',
                entries: [{
                    hint: 'Set background image/video',
                    icon: 'background_image',
                    onclick: ()=>{
                        this.openBackgroundImageDialog()
                    }
                },{
                    hint: 'Set inner',
                    icon: 'inner',
                    onclick: ()=>{
                        this.openInnerImageDialog()
                    }
                },{
                    hint: 'Remove inner',
                    icon: 'noinner',
                    onclick: ()=>{
                        this.removeInner()
                    }
                },{
                    hint: 'Set background color',
                    icon: 'background_color',
                    onclick: ()=>{
                        this.openBackgroundColorDialog()
                    }
                },{
                    hint: 'Set Line color',
                    icon: 'lines',
                    onclick: ()=>{
                        this.openLineColorDialog()
                    }
                },{
                    hint: 'Set Max color',
                    icon: 'max',
                    onclick: ()=>{
                        this.openMaxColorDialog()
                    }
                }]
            }]
        );

        window.requestAnimationFrame(()=>{
            this.draw();
            this.refresh();
        });

        this.core.ui.configurator.on('change', ()=>{
            this.refresh();
        });


        $(window).on('resize',()=>{
            this.refresh();
        });
    }

    /*
        params = {
            file: File // a sound file,
            url: "http://example.org" // is required when using file params,
            title: "Alternative Title" // will be used when file and url is absent, or as fallback when decoding does not work
        }
    */
    buildId3Tags(params){//url, alternativeName){
        let that = this;

        if(params.file){
            try {
                if(!params.url){
                    throw new Error('params.url is missing, but is required when using params.file');
                }
                ID3.loadTags(params.url, function() {
                    let tags = ID3.getAllTags(params.url);
                    tags.title = tags.title?tags.title.trim():''
                    if(!tags.title || !tags.title.length){
                        tags.title = 'File';
                    }
                    that.setId3Tags(tags)
                }, {
                    tags: ["title","artist","album"],
                    dataReader: ID3.FileAPIReader(params.file),
                    onError: _error
                        
                });
            } catch (ex) {
                _error(ex);
            }
        } else if(params.url){
            try {
                if(!params.url){
                    throw new Error('params.url is missing, but is required when using params.file');
                }
                ID3.loadTags(params.url, function() {
                    let tags = ID3.getAllTags(params.url);
                    tags.title = tags.title?tags.title.trim():''
                    if(!tags.title || !tags.title.length){
                        tags.title = 'File';
                    }
                    that.setId3Tags(tags)
                }, {
                    tags: ["title","artist","album"],
                    onError: _error
                        
                });
            } catch (ex) {
                _error(ex);
            }
        } else if(params.title){
            this.setId3Tags({title: params.title});
        } else {
            console.error('did not provide any data to buildId3Tags')
            this.setId3Tags({title: '?'});
        }


        function _error(ex){
            console.warn('error reading id3 tags, using fallback title.', ex);
            that.setId3Tags({title: params.title});
        }
    }

    setId3Tags(tags){
        this.currentTags = tags;
        this.tags.find('.title').html(tags.title||'')
        this.tags.find('.artist').html(tags.artist||'')
        this.tags.find('.album').html(tags.album||'')
        this.adjustTagsSize()

        this.dispatch('tagschange');
    }

    adjustTagsSize(){
        if(this.core.getConfig('show_tags')){
            this.tags.show();
            
            let res = this.calcInnerSize(0)
            this.tags.css('width', res.width).css('height', this.core.getConfig('type') === 'linear'?res.height/3:res.height)

            

            let sw = $(window).width()

            if (this.core.getConfig('adjust_font_size')){
                adjustTagSize(this, this.tags.find('.title'), 1)
                adjustTagSize(this, this.tags.find('.artist'), 0.4)
                adjustTagSize(this, this.tags.find('.album'), 0.4)
            } else {
                setTagMax(this, this.tags.find('.title'), 1, 1)
                setTagMax(this, this.tags.find('.artist'), 0.4, 0.4)
                setTagMax(this, this.tags.find('.album'), 0.4, 0.4)
            }


            function adjustTagSize(that, elem, modifier){
                let min = that.core.getConfig('radius');
                $(elem).css('cssText', '')
                let max_width = min*2*that.core.getConfig('tags_text_width')*modifier

                let font_ratio = JSON.parse($(elem).css('font-size').replace(/[^0-9]/g, '')) / $(elem).width()   
                let new_fontsize = font_ratio*max_width

                $(elem).css('font-size', new_fontsize)
            }

            function setTagMax(that, elem, modifier_width, modifier_size){
                let min = that.core.getConfig('radius');
                let innerWidth = min*2;

                $(elem).css('font-size', that.core.getConfig('tags_text_font_size')*modifier_size+'px')
                $(elem).css('max-width', innerWidth*that.core.getConfig('tags_text_width')*modifier_width+'px')
                if (that.core.getConfig('tags_text_allow_wrap')){
                    $(elem).css('white-space', 'unset')
                } else {
                    $(elem).css('overflow', 'hidden')
                }
            }
        } else {
            this.tags.hide();
        }
    }
    
    openBackgroundImageDialog(){
        new Javis_Dialog({
            title: 'Select image or video for background:',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'file',
                type: 'file',
                short: 'File',
                description: 'Accepts video or images'
            }]
        }, this.dom, (values)=>{
            if(values !== false){
                if(values.file instanceof FileList && values.file[0] instanceof File){
                    this.setBackgroundMedia(values.file[0])
                } else {
                    this.core.ui.showMessage('no file selected');
                }
            }
        })
    }

    openInnerImageDialog(){
        new Javis_Dialog({
            title: 'Select image for inner:',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'file',
                type: 'file',
                short: 'File',
                description: 'Accepts images'
            }]
        }, this.dom, (values)=>{
            if(values !== false){
                if(values.file instanceof FileList && values.file[0] instanceof File){
                    this.setInnerMedia(values.file[0])
                } else {
                    this.core.ui.showMessage('no file selected');
                }
            }
        })
    }

    openBackgroundColorDialog(){
        this.openColorDialog((color, type)=>{
            this.setBackgroundColor(color, type)
        })
    }

    openLineColorDialog(){
        this.openColorDialog((color, type)=>{
            this.setLineColor(color, type)
        }, {radial: false})
    }

    openMaxColorDialog(){
        this.openColorDialog((color, type)=>{
            this.setMaxColor(color, type)
        }, {linear: false, radial: false})
    }

    openColorDialog(/*function*/cb, params){//e.g. params{radial: false}
        let options = []
        if(!params || params.single !== false) options.push({
            value: 'single',
            short: 'single',
            description: 'One color'
        })
        if(!params || params.linear !== false) options.push({
            value: 'linear',
            short: ' gradient',
            description: 'up to 3 colors, with gradient from top to bottom'
        })
        if(!params || params.radial !== false) options.push({
            value: 'radial',
            short: 'Radial gradient',
            description: 'up to 3 colors, with radial gradient.'
        })

        new Javis_Dialog({
            title: 'Select type of color:',
            type: Javis_Dialog.TYPE.OPTION_BUTTONS,
            options: options
        }, this.dom, (value)=>{
            if(value === false) return
            switch(value){
                case 'single': this.openColorSingleDialog(cb)
                    break
                case 'linear': this.openColorLinearDialog(cb)
                    break
                case 'radial': this.openColorRadialDialog(cb)
            }
        })
    }
    
    openColorSingleDialog(/*function*/cb){
        new Javis_Dialog({
            title: 'Select single color',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'color',
                type: 'color',
                short: 'Backgroundcolor',
                description: ''
            }]
        }, this.dom, (values)=>{
            if(values !== false) cb(values.color, 'single')
        })
    }
    
    openColorLinearDialog(/*function*/cb){
        new Javis_Dialog({
            title: 'Select linear color.',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'color0',
                type: 'color',
                short: 'Color stop 0',
                description: 'The bottom color'
            },{
                name: 'color1',
                type: 'color',
                short: 'Color stop 1',
                description: 'Center color, let this empty if you want'
            },{
                name: 'color2',
                type: 'color',
                short: 'Color stop 2',
                description: 'Top color'
            },]
        }, this.dom, (values)=>{
            if(values !== false){
                let arr = []
                for(let key of Object.keys(values)){
                    arr.push(values[key])
                }
                cb(arr, 'linear')
            }
        })
    }
    
    openColorRadialDialog(/*function*/cb){
        new Javis_Dialog({
            title: 'Select radial color.',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'color0',
                type: 'color',
                short: 'Color stop 0',
                description: 'center color'
            },{
                name: 'color1',
                type: 'color',
                short: 'Color stop 1',
                description: 'Middle color, let this empty if you want'
            },{
                name: 'color2',
                type: 'color',
                short: 'Color stop 2',
                description: 'outer color'
            }]
        }, this.dom, (values)=>{
            if(values !== false){
                let arr = []
                for(let key of Object.keys(values)){
                    arr.push(values[key])
                }
                cb(arr, 'radial')
            }
        })
    }
    
    setBackgroundColor(to, type){
        console.log('setting background to', to)
        this.core.setConfig('color_background', to)
        this.core.setConfig('color_background_type', type)
        this.background.css('background', this.toColorString(to, type))
    }


    setBackgroundMedia(fileOrBlob){
        let blobUrl = URL.createObjectURL(fileOrBlob);

        if (fileOrBlob.type.indexOf('image') >= 0){
            this.video.hide()
            console.log('setting background to image ', blobUrl)
            this.background.css('background', "url('"+blobUrl+"') center no-repeat").css('background-size', 'cover');
        } else if (fileOrBlob.type.indexOf('video') >= 0){
            this.background.css('background', '')
            console.log('setting background to video ', blobUrl)

            let video = this.video.get(0);
            video.src = blobUrl;
            video.loop = true;
            video.muted = true;
            video.play();
            this.video.show();
            //check the sizing
            this.video.css('cssText', '')
            setTimeout(()=>{
                if(this.video.width() < this.canvas.width()) this.video.css('cssText', 'width: 100%; height: auto;')

                let overlapX = this.video.width() - this.canvas.width()
                let overlapY = this.video.height() - this.canvas.height()
                console.log('video overlapX', overlapX, 'video overlapY', overlapY, this.video.height(), this.video.width())
                if(overlapX) this.video.css('left', '-' + overlapX/4 + 'px')
                if(overlapY) this.video.css('top', '-' + overlapY/4 + 'px')
            }, 100)
        } else {
            console.error('Only images or videos or blobs are valid for background!', fileOrBlob)
            this.core.ui.showMessage('Only images or videos are valid for background!')
            return
        }

        //send to receiver
        this.core.connector.sendBackgroundBlob(
            fileOrBlob instanceof Blob ? fileOrBlob : (new Blob(fileOrBlob), {type: fileOrBlob.type}),
            fileOrBlob.type
        );
    }
    
    setInnerMedia(file){
        if(file.type.indexOf('image') >= 0){
            this.hasInner = true
            let url = URL.createObjectURL(file)
            console.log('setting inner to image ', url)
            this.inner.css('background', "url('"+url+"') no-repeat").css('background-size', 'cover')
        } else {
            console.error('Only images are valid for inner!', file)
            this.core.ui.showMessage('Only images are valid for inner!')
        }        
    }

    removeInner(){
        this.inner.css('background', '')
        this.hasInner = false
    }

    setLineColor(to, type){
        console.log('setting line color to', to)
        if (type === 'single'){
            this.core.setConfig('color_line', [to, to])
        } else {
            this.core.setConfig('color_line', to)
        }            
    }

    setMaxColor(to, type){
        console.log('setting max color to', to)
        this.core.setConfig('color_max', to)
    }

    selectPreset(val){
        this.core.ui.configurator.selectPreset(val);
    }

    /* canvas stuff */

    draw(){
        if(this.core.isRunning){
            let canvas = this.canvas.get(0);
            let ctx = canvas.getContext('2d');

            let data = this.analyzer.analyzeAudio()
            let arrayL = data.l
            let arrayR = data.r
            let bass = data.bass

            // clear the current state
            ctx.clearRect(0, 0, canvas.width, canvas.height);


            if(this.core.getConfig('bass_smash') && bass > 255*this.core.getConfig('bass_smash_limit')){
                this.inner.addClass('smashing')
                this.tags.addClass('smashing')
            } else {
                this.inner.removeClass('smashing')
                this.tags.removeClass('smashing')
            }

            let numberOfLines = this.core.getConfig('number_of_lines');

            let smoothL = this.postProcessAudioArray(arrayL, numberOfLines);
            let smoothR = this.postProcessAudioArray(arrayR, numberOfLines);

            switch(this.core.getConfig('type')){
                case 'graph':
                    this.drawGraph(smoothL, this.core.getConfig('offset_left'), 180, bass); 
                    this.drawGraph(smoothR, this.core.getConfig('offset_right'), 180, bass, true);  
                    break;
                case 'linear':
                    this.drawLinear(smoothL);
                    break;
                case 'graph_with_lines':
                    this.drawGraph(smoothL, this.core.getConfig('offset_left'), 180, bass); 
                    this.drawGraph(smoothR, this.core.getConfig('offset_right'), 180, bass, true);
                case 'lines':
                default:
                    this.drawCircle(smoothL, this.core.getConfig('offset_left'), 180, bass);    
                    this.drawCircle(smoothR, this.core.getConfig('offset_right'), 180, bass, true);  
                    break;
            }

            if(this.hasInner){
                let res = this.calcInnerSize(bass)
                this.inner.css('width', res.width).css('height', res.height)
            }
        }

        window.requestAnimationFrame(this.draw.bind(this));
    }

    calcInnerSize(bass){
        let calced_width = (this.core.getConfig('radius')+(bass*this.core.getConfig('bass_multiplier')))*2
        let calced_height = (this.core.getConfig('radius')+(bass*this.core.getConfig('bass_multiplier')))*2
        return {width: calced_width, height: calced_height}  
    }

    /* smoothens array and adjusted size of array */
    postProcessAudioArray(arr, size){
        let ret = [];

        for(let i=0; i < size; i++){
            let startIndex = Math.floor(i/size * arr.length);
            let endIndex = Math.floor(i/size * arr.length + 1);

            let sum = 0;
            for(let j = startIndex; j <= endIndex; j++){
                sum += arr[j];
            }

            let val = sum / (endIndex - startIndex);
            ret[i] = val;
        }

        if (this.core.getConfig('line_smoothness') > 0){
            let smoothed = [];

            let mult = this.core.getConfig('line_smoothness');

            let m0 = 1;
            let m1 = Math.max(0, 1 - (1 / mult));
            let m2 = Math.max(0, 1 - (1 / mult) * 2);

            let mTotal = m2 + m1 + m0 + m1 + m2;

            for (let i = 0; i < ret.length; i++) {
                smoothed[i] = ( (ret[i-2]||0) * m2 + (ret[i-1]||0) * m1 + ret[i] + (ret[i+1]||0) * m1 + (ret[i+2]||0) * m2 ) / mTotal;
            }

            return smoothed;
        } else {
            return ret;
        }
    }


    refresh(){
        console.log('refreshing...')
        let bgc = this.toColorString(
            this.core.getConfig('color_background'),
            this.core.getConfig('color_background_type')
        )

        //if((this.background.attr('style')||'').indexOf('url')<0){

            if(this.core.getConfig('background_url')){
               this.background.css('background', "url('"+this.core.getConfig('background_url')+"') center no-repeat").css('background-size', 'cover')
            } else {
                this.background.css('background', bgc)
            }
        //}
        this.dom.attr('type', this.core.getConfig('type'))
        this.adjustTagsSize()
        this.checkCanvasSize(this)
        this.drawCircle(new Array(this.core.getConfig('number_of_lines')), 0, 360, 0)
    }

    
    checkCanvasSize(that) {
        that.canvas.attr('height', $(that.canvas).parent().height())
        that.canvas.attr('width', $(that.canvas).parent().width())
        that.canvasMin = that.canvas.get(0).height > that.canvas.get(0).width ? that.canvas.get(0).height : that.canvas.get(0).width
    }

    /* drawing */

    drawCircle(array, offset, deg_range, bass, counterclockwise){
        let radius = this.core.getConfig('radius')+(bass*this.core.getConfig('bass_multiplier'))
        
        for ( let i = 0; i < (array.length); i++ ){
            let value = array[i]||0;

            let degree = counterclockwise?(offset+(deg_range-(deg_range/array.length*i))):(offset+(deg_range/array.length*i))
            this.drawLine(degree,
                radius,
                value,
                value*this.core.getConfig('value_multiplier'),
                255*this.core.getConfig('mark_max_limit'),
                /* FOR DEBUG PURPOSES, highlights bass_upper_limit red // i <= parseInt(this.core.getConfig('bass_upper_limit')/this.analyzer.WANTED_FREQUENCY_RANG_PER_VALUE) */
            )
        }
    }
    
    drawGraph(array, offset, deg_range, bass, counterclockwise){
        let canvas = this.canvas.get(0);
        let ctx = canvas.getContext('2d');

        let center = {
            x: canvas.width/2,
            y: canvas.height/2
        }
        
        let radius = this.core.getConfig('radius')+(bass*this.core.getConfig('bass_multiplier'))
        ctx.beginPath()
        ctx.lineJoin = 'round';
        ctx.lineWidth = this.core.getConfig('line_width');
    
        for ( let i = 0; i < (array.length); i++ ){
            let value = array[i]||0;

            let degree = counterclockwise?(offset+(deg_range-(deg_range/array.length*i))):(offset+(deg_range/array.length*i))
            let length = value*this.core.getConfig('value_multiplier')
            if(length <= this.core.getConfig('line_length_min')) length = this.core.getConfig('line_length_min')            
            
            let radians = degree * Math.PI / 180
            let start_x = center.x + (Math.cos(radians) * radius)
            let start_y = center.y + (Math.sin(radians) * radius)

            let end_x = start_x + (Math.cos(radians) * length)
            let end_y = start_y + (Math.sin(radians) * length)
            if(i == 0){
                ctx.moveTo(end_x, end_y)
            } else {                
                ctx.lineTo(end_x, end_y)
            }
        }       
        let max = 255*this.core.getConfig('value_multiplier')
        let lineGradient = ctx.createLinearGradient(0, center.y+radius+max, 0, center.y-radius-max);
        let i = 0
        let line_color = this.core.getConfig('color_line')
        for(let color of line_color){
            try {
                lineGradient.addColorStop(i/(line_color.length-1), color);
            } catch (thrown){}

            i++
        }
        ctx.strokeStyle = lineGradient
        ctx.stroke()
    }

    /* if debugMark = true, then will color red */
    drawLine(degree, radius, value, length, max, debugMark){
        let canvas = this.canvas.get(0);
        let ctx = canvas.getContext('2d');

        if(length <= this.core.getConfig('line_length_min')) length = this.core.getConfig('line_length_min')

        let center = {
            x: canvas.width/2,
            y: canvas.height/2
        }

        let line_max = 255*this.core.getConfig('value_multiplier')

        let radians = degree * Math.PI / 180

        let start_x = center.x + (Math.cos(radians) * radius)
        let start_y = center.y + (Math.sin(radians) * radius)
        ctx.beginPath()
        ctx.lineWidth = this.core.getConfig('line_width');
        ctx.moveTo(start_x, start_y)
        let end_x = start_x + (Math.cos(radians) * length)
        let end_y = start_y + (Math.sin(radians) * length)
        ctx.lineTo(end_x, end_y)
        
        if(debugMark){
            ctx.strokeStyle = '#ff0000';
        } else {
            let lineGradient
            if(this.core.getConfig('color_single_line')){
                lineGradient = ctx.createLinearGradient(start_x, start_y, end_x, end_y);
            } else {
                lineGradient = ctx.createLinearGradient(0, center.y + radius + line_max/2, 0, center.y - line_max/2 - radius);
            }
            let i = 0
            let line_color = this.core.getConfig('color_line')
            for(let color of line_color){
                try {
                    lineGradient.addColorStop(i/(line_color.length-1), color);
                } catch (thrown){}

                i++;
            }

            if(this.core.getConfig('mark_max') && this.core.getConfig('color_single_line') && value>max){
                let above = Math.abs(max - value)
                let part = Math.round(above/max*1000)/1000
                //let part_clean = Math.log(part+2.05) / Math.log(2.05) - 0.9
                let part_clean = Math.log(part+2.05) / Math.log(2.45) - 0.7
                lineGradient = ctx.createLinearGradient(start_x, start_y, end_x, end_y);
                let i = 0
                for(let color of line_color){
                    try {
                        lineGradient.addColorStop((1-part_clean-0.001)*i/(line_color.length-1), color);
                    } catch (thrown){}

                    i++
                }
                lineGradient.addColorStop(1, this.core.getConfig('color_max'));
            }
            ctx.strokeStyle = lineGradient
        }

        ctx.stroke()
    }

    drawLinear(array){
        let canvas = this.canvas.get(0);
        let ctx = canvas.getContext('2d');

        let width = this.core.getConfig('radius')*2
        let max_height = 255*this.core.getConfig('value_multiplier')

        let max = 255*this.core.getConfig('mark_max_limit')

        let center = {
            x: canvas.width/2,
            y: canvas.height/2
        }
        let left = center.x - width/2
        let bottom = center.y + max_height/2
        let step_x = width/array.length

        for ( let i = 0; i < (array.length); i++ ){
            let value = array[i]||0;
            let length = value*this.core.getConfig('value_multiplier')
            if(length <= this.core.getConfig('line_length_min')) length = this.core.getConfig('line_length_min')


            let start_x = left + step_x*i
            let start_y = bottom

            let end_x = start_x
            let end_y = start_y - length

            ctx.beginPath()
            ctx.lineWidth = this.core.getConfig('line_width');
            ctx.moveTo(start_x, start_y)

            ctx.lineTo(end_x, end_y)

            let lineGradient;
            if(this.core.getConfig('color_single_line')){
                lineGradient = ctx.createLinearGradient(start_x, start_y, end_x, end_y);
            } else {
                lineGradient = ctx.createLinearGradient(0, center.y + max_height/2, 0, center.y - max_height/2);
            }
            let ci = 0
            let line_color = this.core.getConfig('color_line')
            for(let color of line_color){
                try {
                    lineGradient.addColorStop(ci/(line_color.length-1), color);
                } catch (thrown){}

                ci++
            }

            if(this.core.getConfig('mark_max') && this.core.getConfig('color_single_line') && value>max){
                let above = Math.abs(max - value)
                let part = Math.round(above/max*1000)/1000
                //let part_clean = Math.log(part+2.05) / Math.log(2.05) - 0.9
                let part_clean = Math.log(part+2.05) / Math.log(2.45) - 0.7
                lineGradient = ctx.createLinearGradient(start_x, start_y, end_x, end_y);
                let cci = 0
                for(let color of line_color){
                    try {
                        lineGradient.addColorStop((1-part_clean-0.001)*cci/(line_color.length-1), color);
                     } catch (thrown){}

                    cci++
                }
                lineGradient.addColorStop(1, this.core.getConfig('color_max'));
            }

            ctx.strokeStyle = lineGradient
            ctx.stroke()

        }
    }

    toColorString(to, type){
        let str
        if(type === 'radial') {
            str = 'radial-gradient(circle, '
            let i = 0
            for(let color of to){
                str += color+' '+(i/(to.length-1))*100+'%'+(i<to.length-1?', ':'')
                i++
            }
            str += ')'
        } else if(type === 'linear') {
            str = 'linear-gradient(to bottom, '
            let i = 0
            for(let color of to){
                str += color+' '+(i/(to.length-1))*100+'%'+(i<to.length-1?', ':'')
                i++
            }
            str += ')'
        } else {
            str = to
        }  
        return str
    }    
}